
# CoD4 Mods

CoD4 Mods that I make for fun.



## Custom Models

1. CheyTac M200 (R700 custom replacement mod) 
    Download: https://drive.google.com/file/d/1gU--yC8ruE4xjrnJqxVuLDci_1hiuzjA/view

2. M40A3 Modern Warfare 4 Remastered (M40A3 custom replacement mod)
    Download: https://drive.google.com/file/d/1XcNalFmnKSWobHPsPU440vl9tkacgbPX/view

3. Word At War Gun Pack (Lot of guns have been replaced with custom models)
    Download: https://drive.google.com/file/d/1qzNoMbfqiJbYBBnZVOG8zYCT6CQrixP7/view?usp=sharing
